#include "paging.h"
#include "common.h"
#include "kheap.h"
#include "monitor.h"
#include "task.h"
#include "scheduler.h"

extern uint32_t initial_esp;
extern page_directory_t* current_directory;

extern void alloc_frame(page_t*, int, int);
extern void switch_task();
extern void user_main();
extern void call_fork();
extern void trigger_scheduler();

#define STACK_START 0xE0000000

task_t* ready_queue;
task_t* current_task;
task_t* prev_task;

int next_pid = 0;

void set_current_task(task_t* task)
{
  prev_task = current_task;
  current_task = task;
}

void exit_task() {
  // This allows tasks to exit gracefully
  mon_var("This process is exiting", current_task->id);
  // set the state of the process
  current_task->state = DONE;
  // Just loop until the scheduler runs.
  for (;;);
}

task_t* spawn_kthread(uint32_t func_ptr, uint32_t add_to_queue)
{
  task_t* task = (task_t*)kmalloc(sizeof(task_t));

  task->next = NULL;
  task->id = next_pid++;
  task->pd = current_directory; //  Needs to change later
  task->state = READY;
  task->child = NULL;
  task->parent = NULL;

  // Create the kernel stack
  task->k_stack = (uint32_t)kmalloc_a(KSTACK_SIZE) + KSTACK_SIZE - 8;

  uint32_t *stack, *base;
  stack = base = (uint32_t*)task->k_stack;

  // Initialize the stack
  *stack = (uint32_t)&exit_task;
  *--stack = func_ptr; // eip
  *--stack = 0; // eax
  *--stack = 0; // ecx
  *--stack = 0; // edx
  *--stack = 0; // ebx
  *--stack = 0; // esp
  *--stack = (uint32_t)base; // ebp
  *--stack = 0; // esi
  *--stack = 0; // edi
  *--stack = 0x10; // ds
  *--stack = 0x10; // fs
  *--stack = 0x10; // es
  *--stack = 0x10; // gs

  // This line here sets the stack pointer stored on the stack,
  // If we didn't do this the esp would be messed up when the
  // Task switch executes popad
  *(stack + 7) = (uint32_t)stack;

  // Set the new stack pointer
  task->k_esp = (uint32_t)stack;
  task->k_ebp = (uint32_t)base;

  if (add_to_queue)
  {
    // Add thread to ready queue
    add_task_to_queue(task, &ready_queue);
  }

  return task;
}

void test() {
  task_t* idle = get_task_by_pid(0, ready_queue);
  printv("\nidle esp", idle->k_esp);

  user_main();
}

void idle_task()
{
  task_t* task = spawn_kthread((uint32_t)user_main, true);

  set_current_task(task);
  switch_task();
  println("I'm back");

  for(;;);
}

void initialize_tasking()
{
	asm volatile("cli");
	// Relocate the stack so we know where it is.
	move_stack((void*)STACK_START, KSTACK_SIZE);

	// Create a task for the kernel
	task_t* kernel_task = spawn_kthread((uint32_t)idle_task, true);
  current_task = (task_t*)kmalloc(sizeof(task_t));

	// Set the kernel task to be the current task
  set_current_task(kernel_task);

  asm volatile("sti");

  switch_task();
}

int kgetpid() {
	return current_task->id;
}

void add_task_to_queue(task_t* task, task_t** queue)
{

  if (task == NULL)
    return; // Avoid null pointers

  if (*queue == NULL) {
    *queue = task;
    return;
  }

  task_t* temp = *queue;
  while (temp->next != NULL)
  {
    temp = temp->next;
  }

  temp->next = task;
}

void copy_stack(task_t* dst_task, task_t* src_task)
{
  uint32_t dst_stack = dst_task->k_stack;
  uint32_t src_stack = src_task->k_stack;

  uint32_t offset = dst_stack - src_stack;

  uint32_t new_esp = src_task->k_esp + offset;
  uint32_t new_ebp = src_task->k_ebp + offset;

  // memcpy((void*)new_esp, (void*)src_task->k_esp, src_stack - src_task->k_esp);
  memcpy((void*)dst_stack - (KSTACK_SIZE - 8), (void*)src_stack - (KSTACK_SIZE - 8), KSTACK_SIZE - 8);

  // Backtrace through the original stack, copying
  // new values into the new stack
  uint32_t i;
  for (i = dst_stack; i > src_task->k_esp; i -= 4)
  {
    // The value stored at i
    uint32_t tmp = *(uint32_t*)i;

    // remap anything that pointed within the stack
    // to point to reference the new stack
    if ((src_task->k_esp < tmp) && (tmp < src_stack))
    {
      tmp = tmp + offset;
      uint32_t* tmp2 = (uint32_t*)i;
      *tmp2 = tmp;
    }
  }

  dst_task->k_esp = new_esp;
  dst_task->k_ebp = new_ebp - (src_stack - src_task->k_esp);
}

int kfork(regs_t regs) {
	asm volatile("cli");

  // Get parent
  task_t* parent = current_task;

  // Create a new task
  task_t* new_task = spawn_kthread(NULL, false);
  copy_stack(new_task, parent);

  // Set as child of parent (Congrats to the current task on their new child!)
  add_child(parent, new_task);

  // Set the task's parent
  new_task->parent = parent;

  // Setup that stack, bud
  uint32_t* stack = (uint32_t*)new_task->k_ebp;

  // Let's find out what is around the next ebp
  uint32_t* new_esp = (uint32_t*)*stack;

  // set the address for the exit task
  uint32_t* ret_addr_loc = ((uint32_t*)*new_esp) + 1;
  *ret_addr_loc = (uint32_t)exit_task;

  // Push the registers to the stack.
  *stack = 0; // return value
  *--stack = regs.ecx;
  *--stack = regs.edx;
  *--stack = regs.ebx;
  *--stack = (uint32_t)new_esp; // esp
  *--stack = *new_esp; // ebp;
  *--stack = regs.esi;
  *--stack = regs.edi;
  *--stack = 0x10; // ds
  *--stack = 0x10; // es
  *--stack = 0x10; // fs
  *--stack = 0x10; // gs

  new_task->k_esp = (uint32_t)stack;
  // TODO Clone parent pd

  // Add task to queue
  add_task_to_queue(new_task, &ready_queue);

  asm volatile("sti");
  // Exit
  if (current_task->id == new_task->id) {
    // This is the child
    return 0;
  }
  else {
    return new_task->id;
  }
}

void move_stack(void *new_stack_start, uint32_t size)
{
	uint32_t i;
	for( i = (uint32_t)new_stack_start; i >= ((uint32_t)new_stack_start-size); i -= 0x1000)
	{
		// General-purpose stack is in user-mode.
		alloc_frame( get_page(i, 1, current_directory), 0 /* User mode */, 1 /* Is writable */ );
	}

	// Flush the TLB by reading and writing the page directory address again.
	uint32_t pd_addr;
	asm volatile("mov %%cr3, %0" : "=r" (pd_addr));
	asm volatile("mov %0, %%cr3" : : "r" (pd_addr));

	// Old ESP and EBP, read from registers.
	uint32_t old_esp;
	asm volatile("mov %%esp, %0" : "=r" (old_esp));
	uint32_t old_ebp;
	asm volatile("mov %%ebp, %0" : "=r" (old_ebp));

	uint32_t offset = (uint32_t)new_stack_start - initial_esp;

	uint32_t new_esp = old_esp + offset;
	uint32_t new_ebp = old_ebp + offset;

	memcpy((void*)new_esp, (void*)old_esp, initial_esp - old_esp);

	// Backtrace through the original stack, copying
	// new values into the new stack
	for (i = (uint32_t)new_stack_start; i > (uint32_t)new_stack_start - size; i -= 4)
	{
		// The value stored at i
		uint32_t tmp = *(uint32_t*)i;

		// remap anything that pointed within the stack
		// to point to reference the new stack
		if ((old_esp < tmp) && (tmp < initial_esp))
		{
			tmp = tmp + offset;
			uint32_t* tmp2 = (uint32_t*)i;
			*tmp2 = tmp;
		}
	}

	asm volatile("mov %0, %%esp"::"r"(new_esp));
	asm volatile("mov %0, %%ebp"::"r"(new_ebp));
}

void add_child(task_t* parent, task_t* child)
{
  // Is the parent childless?
  if (parent->child == NULL)
    parent->child = child; // That was easy
  else
  { // This parent already has kiddos
    // Find the last child
    task_t* temp_child = parent->child;
    while (temp_child->sibling != NULL)
      temp_child = temp_child->sibling;

    // Add the new child to the sibling chain
    temp_child->sibling = child;
  }
}

uint32_t has_siblings(task_t* task)
{
  if (!is_child(task))
    return 0; // If it isn't a child it doesn't have siblings

  if (task->sibling != NULL)
    return 1; // Easy way to tell if there is siblings

  // It might be the youngest sibling though...or an only child
  if (task->parent->child != task)
    return 1; // It is the youngest, AKA the best child
  else
    return 0; // It is an only child
}

uint32_t is_child(task_t* task)
{
  return task->parent != NULL;
}

void remove_from_family(task_t* task)
{
  if (!is_child(task))
    return; // Can't remove it from a family it never had

  task_t* parent = task->parent;
  task_t* oldest_child /* AKA entitled */ = parent->child;

  if (oldest_child == task)
  {
    // we are the oldest, this is easy
    if (has_siblings(task))
      parent->child = task->sibling; // Replace the oldest
    else
      parent->child = NULL; // :'(
  }
  else
  {
    // We ain't the oldest, so we have to do things the hard way
    task_t* temp_child = oldest_child;

    // Loop through all of children
    while (temp_child != NULL)
    {
      // If we find the target child coming up next...
      if (temp_child->sibling == task) {
        temp_child->sibling = task->sibling; // Remove the child from the family
        break;
      }

      temp_child = temp_child->sibling;
    }
  }
}

void free_task(task_t* task)
{
  if (task == NULL)
    return; // Avoid null pointers

  if (is_child(task))
      // Remove it from the family
      remove_from_family(task);


  // free_page_directory(task->pd);

  // Free the stack
  kfree((void*)(task->k_stack - KSTACK_SIZE + 8));

  kfree(task);
}

task_t* get_task_by_pid(int pid, task_t* queue)
{
  task_t* temp = queue;
  while(temp != NULL && temp->id != pid)
    temp = temp->next;

  return temp;
}
