#include "kernel_ken.h"
#include "kheap.h"
#include "monitor.h"
#include "task.h"

extern heap_t* kheap;
extern task_t* current_task;
// Defined in process.s
extern int call_fork();

void print(const char* str) {
	monitor_write(str);
}

void print_hex(unsigned int n) {
	monitor_write_hex(n);
}

void print_var(const char* prefix, unsigned int n) {
	print(prefix);
	print(": ");
	print_hex(n);
	print("\n");
}

// Print an unsigned integer to the monitor in base 10
void print_dec(unsigned int n) {
	monitor_write_dec(n);
}

void* alloc(unsigned int size, unsigned char page_align) {
	return malloc(size, page_align, kheap);
}

int fork() {
	int retval = call_fork();
	return retval;
}

int getpid() {
	return (int)kgetpid();
}
