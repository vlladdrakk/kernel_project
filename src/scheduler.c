#include "common.h"
#include "task.h"
#include "kheap.h"
#include "monitor.h"
#include "scheduler.h"

extern task_t* current_task;
extern task_t* ready_queue;

extern uint32_t get_esp();
extern void switch_task();

void cleanup_queue(task_t** queue)
{
  if (*queue == NULL || queue == NULL)
    return; // Avoid null pointers

  task_t* task = *queue;
  while (task != NULL)
  {
    // Handle DONE task
    if (task->state == DONE)
    {
      // Check for children
      if (task->child != NULL)
      {
        // There are children, this task should now be a zombie
        task->state = ZOMBIE;
      }
      else
      {
        // No children, we can remove the task
        remove_task_from_queue(task, queue);
        free_task(task);
      }
    }
    // Handle ZOMBIE task
    else if (task->state == ZOMBIE)
    {
      // Does it still have children?
      if (task->child == NULL)
      {
        // No children means we can remove the task
        remove_task_from_queue(task, queue);
        free_task(task);
      }
    }

    task = task->next;
  }
}

void remove_task_from_queue(task_t* task, task_t** queue)
{
	// If the task has children, we wait for the children to die
	if (task->child != NULL)
	{
		task->state = ZOMBIE;
		return;
	}

	task_t* temp = *queue;

	while (temp != NULL)
	{
		// if the next task is the target
		if (temp->next == task)
		{
			temp->next = task->next; // Remove from the linked list
			break;
		}

		temp = temp->next;
	}
}

uint32_t queue_len(task_t** queue)
{
  if (*queue == NULL)
    return 0;

  task_t* temp = *queue;
  uint32_t len = 0;
  while (temp != NULL)
  {
    temp = temp->next;
    len++;
  }

  return len;
}

task_t* get_next_task()
{
	// If the idle task is the only task left, then we switch to it
	if (ready_queue->next == NULL)
		return ready_queue;

	task_t* temp = ready_queue;
	while (temp != NULL)
	{
		// Looking for something ready that isn't the idle task
		if (temp->id != 0 && temp->state == READY)
		{
			return temp;
			break;
		}

		temp = temp->next;
	}

  // Could not find a task try cleaning up the queue again.
  cleanup_queue(&ready_queue);

  if (queue_len(&ready_queue) == 1)
    return ready_queue;

	PANIC("No more tasks???");
	return NULL;
}

void schedule(registers_t* regs)
{
	// Don't do anything before initialize tasking
  if (current_task == NULL || (current_task == ready_queue && current_task->next == NULL)) {
    return;
  }
  asm volatile("cli");

  cleanup_queue(&ready_queue);

  // Update current task state
  if (current_task->state == RUNNING) {
  	current_task->state = READY;
  }

  // Get next task
  set_current_task(get_next_task());

  asm volatile("sti");

  // Load new registers
  switch_task();
}