#include "kernel_ken.h"
#include "common.h"
#include "user_main.h"
#include "monitor.h"

extern uint32_t get_eip();

void user_main() {
	uint32_t* a = (uint32_t*) alloc(sizeof(uint32_t), 0);
	*a = 10;
	// print_dec(*a);
	// print("\nmemory allocation worked!\n");

	uint32_t ret = fork();

	print("fork() returned ");
	print_hex(ret);
	print(", and getpid() returned ");
	print_hex(getpid());
	print("\n============================================================================\n");
	if (ret == 0) {
		println("i am child");
		uint32_t pid = fork();
		if (pid == 0) {
			println("another child");
		} else {
			println("first child");
		}
	}
}
