// main.c -- Defines the C-code kernel entry point, calls initialisation routines.
//           Made for JamesM's tutorials <www.jamesmolloy.co.uk>

#include "monitor.h"
#include "descriptor_tables.h"
#include "timer.h"
#include "paging.h"
#include "main.h"
#include "multiboot.h"
#include "kheap.h"
#include "user_main.h"
#include "task.h"
#include "handlers.h"

uint32_t initial_esp;

int kernel_main(multiboot_t *mboot_ptr, uint32_t esp)
{
	// Set the initial stack pointer so it can be moved.
	initial_esp = esp;

  init_descriptor_tables();

  monitor_clear();

  register_handlers();

  asm volatile("sti");
	init_timer(50);

	initialise_paging();

	initialize_tasking();

  return 0;
}
