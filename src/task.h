#ifndef TASK_H
#define TASK_H

#include "paging.h"

#define KSTACK_SIZE 0x2000

// Define process states
#define DONE		0
#define READY		1
#define RUNNING 2
#define ZOMBIE	3

typedef struct _task_t
{
	uint32_t k_esp;
	uint32_t k_ebp;
	uint32_t state;
  uint32_t k_stack;
  int id;
  page_directory_t* pd;
  struct _task_t* next;
  struct _task_t* child;
  struct _task_t* parent;
  struct _task_t* sibling;
} task_t;

typedef struct  {
	uint32_t edi;
	uint32_t esi;
	uint32_t ebp;
	uint32_t esp;
	uint32_t ebx;
	uint32_t edx;
	uint32_t ecx;
	uint32_t eax;
} regs_t;

void initialize_tasking();

void move_stack(void *new_stack_start, uint32_t size);

int kfork(regs_t regs);

int kgetpid();

void free_task(task_t* task);

void add_child(task_t* parent, task_t* child);

uint32_t has_siblings(task_t* task);

uint32_t is_child(task_t* task);

void add_task_to_queue(task_t* task, task_t** queue);

void print_regs(regs_t regs);

void set_current_task(task_t* task);

task_t* get_task_by_pid(int pid, task_t* queue);

#endif