#include "monitor.h"
#include "isr.h"

void invalid_opcode(registers_t *regs)
{
    monitor_write("Invalid Opcode!\nebp: ");
    monitor_write_hex(regs->ebp);
    monitor_write(" - EIP: ");
    monitor_write_hex(regs->eip);
    monitor_write("\nESP: ");
    monitor_write_hex(regs->esp);
    monitor_write("\n");
    PANIC("Invalid Opcode");
}

void gp_fault(registers_t *regs)
{
  monitor_write("General protection fault!\n");
  printv("err_code", regs->err_code);
  printv("eip", regs->eip);
  printv("esp", regs->esp);
  printv("ebp", regs->ebp);
  PANIC("GP fault");
}

void out_of_bounds(registers_t* regs)
{
  monitor_write("out of bounds exception\nEIP = ");
  monitor_write_hex(regs->eip);
  monitor_write("\n");
  PANIC("Out of bounds");
}

void breakpoint_handler(registers_t* regs)
{
  printv("Breakpoint interrupt\neip", regs->eip);
  printv("err_code", regs->err_code);
  printv("esp", regs->esp);
  printv("ebp", regs->ebp);
  PANIC("Breakpoint fault");
}

void register_handlers()
{
  register_interrupt_handler(6, invalid_opcode);
  register_interrupt_handler(13, gp_fault);
  register_interrupt_handler(5, out_of_bounds);
  register_interrupt_handler(3, breakpoint_handler);
}
