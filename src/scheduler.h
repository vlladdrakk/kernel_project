#ifndef SCHEDULER_H
#define SCHEDULER_H

void schedule(registers_t* regs);

void remove_task_from_queue(task_t* task, task_t** queue);

void cleanup_queue(task_t** queue);

#endif