[GLOBAL copy_page_physical]
copy_page_physical:
    push ebx              ; According to __cdecl, we must preserve the contents of EBX.
    pushf                 ; push EFLAGS, so we can pop it and reenable interrupts
                          ; later, if they were enabled anyway.
    cli                   ; Disable interrupts, so we aren't interrupted.
                          ; Load these in BEFORE we disable paging!
    mov ebx, [esp+12]     ; Source address
    mov ecx, [esp+16]     ; Destination address

    mov edx, cr0          ; Get the control register...
    and edx, 0x7fffffff   ; and...
    mov cr0, edx          ; Disable paging.

    mov edx, 1024         ; 1024*4bytes = 4096 bytes

.loop:
    mov eax, [ebx]        ; Get the word at the source address
    mov [ecx], eax        ; Store it at the dest address
    add ebx, 4            ; Source address += sizeof(word)
    add ecx, 4            ; Dest address += sizeof(word)
    dec edx               ; One less word to do
    jnz .loop

    mov edx, cr0          ; Get the control register again
    or  edx, 0x80000000   ; and...
    mov cr0, edx          ; Enable paging.

    popf                  ; Pop EFLAGS back.
    pop ebx               ; Get the original value of EBX back.
    ret

[GLOBAL switch_task]
[extern current_task]
[extern prev_task]
[extern monitor_write_hex]
switch_task:
  cmp dword [prev_task], 0
  je .perform_switch ; Don't record esp if the previous task is null

  push eax ; save eax
  mov eax, [prev_task]
  mov [eax], esp    ; record esp for previous stack
  mov [eax+4], ebp
  pop eax ; restore the saved eax

.perform_switch:
  pushad ; push all regs
  push ds
  push es
  push fs
  push gs

  ; change stacks
  mov eax, [current_task]
  mov esp, [eax] ; Moves the esp of the new task into esp
  ; Pop the registers for the new process
  ; call monitor_write_hex
  pop gs
  pop fs
  pop es
  pop ds
  popad
  ret

[GLOBAL call_fork]
[extern kfork]
call_fork:
    ; So here is the idea, we add the registers as parameters for kfork
    pushad
    call kfork  ; Now we call kfork to do the fork
    add esp, 0x20  ; Here we clean up the stack
    ret           ; And BAM we return the return value of fork.
